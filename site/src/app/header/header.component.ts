import { Component, OnInit } from '@angular/core';
import { UsersService } from '../shared/users.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public ui_user_type="Business User";
  constructor(public service:UsersService) { }

  ngOnInit(): void {
  }

  onSubmit(form:NgForm)
  {
    this.insertRecord(form);
  }
  insertRecord(form:NgForm)
  {
    this.service.postUsers().subscribe(
      res => {
        console.log(res);
      },
      err => {
        console.log(err);
      }
    )
  }
}
