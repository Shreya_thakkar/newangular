import { Injectable } from '@angular/core';
import { Users } from './users';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  public formData = {} as Users;
  readonly rootURL='http://localhost:55754/api';
  public ui_user_type='Business User';
  constructor(public http:HttpClient) { }

  postUsers()
  {
    return this.http.post(this.rootURL +'/values',this.formData);
  }
}
