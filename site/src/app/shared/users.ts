export class Users {
    id:number;
    name:string;
    email:string;
    mobile:string;
    password:string;
    c_password:string;
    ui_user_type:'Business User';
}
